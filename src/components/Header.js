import React, { Component } from "react";
import { FcFlashOn, FcFlashOff } from "react-icons/fc";
import { Link } from "react-router-dom";
import { BrowserRouter } from "react-router-dom";
import Battle from "./Battle";
export default class Header extends Component {
  constructor() {
    super();
    this.state = {
      light: true,
      popular: true,
      Battle: false,
    };
  }

  themeChangeEvent = () => {
    this.setState({
      light: !this.state.light,
    });
    this.props.lightChanger(this.state.light);
  };
  popularClass = (e) => {
    this.setState({
      popular: true,
      Battle: false,
    });
  };
  battleClass = (e) => {
    this.setState({
      popular: false,
      Battle: true,
    });
  };
  render() {
    return (
      <header className="main-header">
        <div className="routing-names">
          <Link to={"/"}>
            <p
              className={this.state.popular ? "color-change" : "popular"}
              onClick={(e) => this.popularClass(e)}
            >
              Popular
            </p>
          </Link>
          <Link to={"/Battle"}>
            <p
              className={this.state.Battle ? "color-change" : "Battle"}
              onClick={(e) => this.battleClass(e)}
            >
              Battle
            </p>
          </Link>
        </div>
        <div>
          {this.state.light && (
            <FcFlashOn
              className="light-icon"
              onClick={() => this.themeChangeEvent()}
            />
          )}
          {!this.state.light && (
            <FcFlashOff
              className="light-icon"
              onClick={() => this.themeChangeEvent()}
            />
          )}
        </div>
      </header>
    );
  }
}
