import React, { Component } from "react";
import fetch from "node-fetch";
import PopularCards from "./PopularCards";
import Header from "./Header";

export default class Popular extends Component {
  constructor() {
    super();
    this.state = {
      language: "",
      data: [],
      isAvailable: false,
    };
  }

  data = async (language) => {
    let fetchedData = await fetch(
      `https://api.github.com/search/repositories?q=stars:%3E1+language:${language}&sort=stars&order=desc&type=Repositories`
    );
    let jsonData = await fetchedData.json();
    this.setState({
      language: language,
      data: jsonData,
      isAvailable: true,
    });
  };

  componentDidMount() {
    this.data("All");
  }

  clickHandler(value) {
    this.setState({
      isAvailable: false,
    });
    this.data(value);
  }
  render() {
    // console.log(this.state.data.items);
    return (
      <div>
        <div className="buttons">
          <button
            className="language-buttons"
            value="All"
            onClick={(e) => this.clickHandler(e.target.value)}
          >
            All
          </button>
          <button
            className="language-buttons"
            value="JavaScript"
            onClick={(e) => this.clickHandler(e.target.value)}
          >
            JavaScript
          </button>
          <button
            className="language-buttons"
            value="Ruby"
            onClick={(e) => this.clickHandler(e.target.value)}
          >
            Ruby
          </button>
          <button
            className="language-buttons"
            value="Java"
            onClick={(e) => this.clickHandler(e.target.value)}
          >
            Java
          </button>
          <button
            className="language-buttons"
            value="CSS"
            onClick={(e) => this.clickHandler(e.target.value)}
          >
            CSS
          </button>
          <button
            className="language-buttons"
            value="Python"
            onClick={(e) => this.clickHandler(e.target.value)}
          >
            Python
          </button>
        </div>
        {!this.state.isAvailable && (
          <div className="loading-message" role="status">
            Data is being fetched...
          </div>
        )}
        <div className="total-cards">
          {this.state.isAvailable &&
            this.state.data.items.map((el, index) => (
              <PopularCards name={el} id={index} />
            ))}
        </div>
      </div>
    );
  }
}
