import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import { Routes, Route } from "react-router-dom";
import Popular from "./components/Popular";
import Battle from "./components/Battle";
import "./App.css";
import ResetPage from "./components/ResetPage";
import Header from "./components/Header";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      language: "All",
      light: true,
    };
  }
  themeChange = (data) => {
    this.setState({
      light: !data,
    });
  };

  render() {
    return (
      <div className={`${this.state.light}`}>
        <Header lightChanger={this.themeChange} />
        <Routes>
          <Route path="/" element={<Popular />} />
          <Route exact path="/Battle" element={<Battle />} />
        </Routes>
      </div>
    );
  }
}
